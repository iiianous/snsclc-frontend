import React, { useEffect, useState, useContext } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { useForm } from "react-hook-form";

import Main from "../layout/Main";
import ClassDetails from "../../components/ClassDetails";
import NoClass from "../../components/NoClass";
import Modal from "../Modal";
import ModalContext from "../../context/modal-context";

const Subjects = () => {
	let [subjects, setSubjects] = useState([]);
	let { modal, toggleModal } = useContext(ModalContext);
	const { register, errors, handleSubmit } = useForm();

	async function fetchData() {
		const { data } = await axios.get("http://localhost:5000/api/subjects/");
		setSubjects(data);
		console.log({ data });
	}

	const onSubmit = (data) => {
		console.log(data);
		// toggleModal(false);
	};

	useEffect(() => {
		fetchData();
	}, []);

	const openModal = () => {
		toggleModal({ ...modal, teacher: true });
	};

	return (
		<Main>
			<div className="w-full pt-4 flex flex-col">
				<div className="flex items-center justify-center mb-5">
					<h1 className="text-xl sm:text-2xl md:text-4xl">Add a Subject</h1>
				</div>
				<div className="flex flex-row">
					<div className="w-full">
						{/* <pre>{JSON.stringify(subjects, null, 2)}</pre> */}
						{subjects.length ? (
							subjects.map((item, index) => (
								// <p>{item.subject}</p>
								<ClassDetails
									item={item}
									key={index}
									useLink={false}
									toggleModal={toggleModal}
								/>
							))
						) : (
							<NoClass width="300" height="300" />
						)}
					</div>
				</div>
			</div>
		</Main>
	);
};

export default Subjects;
