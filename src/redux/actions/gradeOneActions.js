import moment from "moment";
import store from "../store"

export const getDay = () => {
	const { settingsReducer } = store.getState();

	if (settingsReducer?.day) {
		return settingsReducer?.day
	}

	return moment().format("dddd");
}

export const setClass = (allGradeLevels, currentGrade) => {
	const formattedDay = getDay()

	const classesOfTheDay = allGradeLevels.filter((item) => {
		return item.tag.includes(formattedDay);
	});

	if (!currentGrade) {
		return {
			type: "SET_CLASS",
			payload: { subjects: classesOfTheDay },
		};
	}

	const subjects = classesOfTheDay.filter((item) => {
		return item.grade === currentGrade;
	});

	return {
		type: "SET_CLASS",
		payload: { subjects },
	};
};

export const setGradeLevel = (payload = {}) => {
	return {
		type: "SET_GRADE_LEVEL",
		payload,
	};
};
