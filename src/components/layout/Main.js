import React, { useContext, useState } from "react";

import Navbar from "../Navbar";
import Footer from "../Footer";
import ModalTeacher from "../ModalTeacher";
import ModalSubject from "../ModalSubject";

// import WithModalContext from "../../with-modal-context";
import ModalContext from "../../context/modal-context";

// function Main(WrappedComponents) {
// 	return (props) => (
// 		<div className="min-h-screen relative">
// 			<Navbar />
// 			<div className="bg-gray-300 min-h-screen">
// 				<div className="w-11/12 md:w-3/4 mx-auto">
// 					<WrappedComponents {...props} />
// 				</div>
// 			</div>
// 			<Footer />
// 			{/* <Modal /> */}
// 		</div>
// 	);
// }

function Main({ children }) {
	let { modal, toggleModal } = useContext(ModalContext);

	return (
		<div className="min-h-screen relative">
			<Navbar />
			<div className="bg-gray-300 min-h-screen">
				<div className="w-11/12 md:w-3/4 mx-auto">{children}</div>
			</div>
			{/* <pre>{JSON.stringify(modal, null, 2)}</pre>
			<button
				onClick={() => toggleModal({ teacher: true })}
				className="p-4 text-white uppercase bg-orange-600"
			>
				Toggle Teacher Modal
			</button>
			<button
				onClick={() => toggleModal({ teacher: false, subject: true })}
				className="p-4 text-white uppercase bg-orange-600"
			>
				Toggle Subject Modal
			</button> */}

			{modal.teacher && <ModalTeacher value={modal.value} />}
			{/* {modal.subject && <ModalSubject value={modal.value} />} */}
			<Footer />
		</div>
	);
}

export default Main;
