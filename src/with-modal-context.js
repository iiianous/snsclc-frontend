import React from "react";

import ModalContext from "./context/modal-context";

const withModalContext = (Component) => {
	return (props) => {
		return (
			<ModalContext.Consumer>
				{(context) => <Component {...props} {...context} />}
			</ModalContext.Consumer>
		);
	};
};

export default withModalContext;
