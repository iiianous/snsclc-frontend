import { useEffect, useState } from "react";

export default () => {
	let [statusText, setStatusText] = useState(false);

	function setUpListener(event) {
		const result = navigator.onLine ? "online" : "offline";
		setStatusText(result);
	}

	function addListener() {
		window.addEventListener("online", setUpListener);
		window.addEventListener("offline", setUpListener);
	}

	useEffect(() => {
		setUpListener();
		addListener();

		return () => {
			window.removeEventListener("online", setUpListener);
			window.removeEventListener("offline", setUpListener);
		};
	}, [statusText]);

	return {
		statusText,
	};
};
