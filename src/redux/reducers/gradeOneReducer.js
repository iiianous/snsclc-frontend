const INITIAL_STATE = [
	{
		tag: ["monday", "tuesday"],
		start_time: "7:30",
		end_time: "8:00",
		subject: "English",
		grade: "grade-1",
		meet_link_default: "",
		teacher: {
			name: "Ann",
			meet_links: ["//meet.google.com/phk-auwc-ewa"],
		},
	},
	{
		tag: ["monday", "tuesday"],
		start_time: "8:10",
		end_time: "8:40",
		subject: "AP",
		grade: "grade-1",
		meet_link_default: "",
		teacher: {
			name: "Marianne",
			meet_links: ["//meet.google.com/nza-qjxn-qds"],
		},
	},
	{
		tag: ["monday", "tuesday"],
		start_time: "8:50",
		end_time: "9:20",
		subject: "Filipino",
		grade: "grade-1",
		meet_link_default: "",
		teacher: {
			name: "Kris",
			meet_links: ["//meet.google.com/zpb-dtec-adf"],
		},
	},
	{
		tag: ["monday", "tuesday"],
		start_time: "9:30",
		end_time: "10:00",
		subject: "Math",
		grade: "grade-1",
		meet_link_default: "",
		teacher: {
			name: "Ayen",
			meet_links: ["//meet.google.com/uer-utmy-idc"],
		},
	},
	{
		tag: ["monday", "tuesday"],
		start_time: "10:10",
		end_time: "10:40",
		subject: "ESP",
		grade: "grade-1",
		meet_link_default: "",
		teacher: {
			name: "Kris",
			meet_links: ["//meet.google.com/zpb-dtec-adf"],
		},
	},
	{
		tag: ["wednesday", "thursday"],
		start_time: "8:10",
		end_time: "8:40",
		subject: "MTB",
		grade: "grade-1",
		meet_link_default: "",
		teacher: {
			name: "Ann",
			meet_links: ["//meet.google.com/phk-auwc-ewa"],
		},
	},
	{
		tag: ["wednesday", "thursday"],
		start_time: "9:30",
		end_time: "10:00",
		subject: "MAPEH",
		grade: "grade-1",
		meet_link_default: "",
		teacher: {
			name: "Ayen",
			meet_links: ["//meet.google.com/uer-utmy-idc"],
		},
	},
	{
		tag: ["wednesday", "thursday"],
		start_time: "10:50",
		end_time: "11:20",
		subject: "Science",
		grade: "grade-1",
		meet_link_default: "",
		teacher: {
			name: "Marianne",
			meet_links: ["//meet.google.com/nza-qjxn-qds"],
		},
	},
	{
		tag: ["monday", "tuesday"],
		start_time: "7:30",
		end_time: "8:00",
		subject: "Math",
		grade: "grade-2",
		meet_link_default: "",
		teacher: {
			name: "Ayen",
			meet_links: ["//meet.google.com/jce-qvxe-rom"],
		},
	},
	{
		tag: ["monday", "tuesday"],
		start_time: "8:10",
		end_time: "8:40",
		subject: "English",
		grade: "grade-2",
		meet_link_default: "",
		teacher: {
			name: "Ann",
			meet_links: ["//meet.google.com/phk-auwc-ewa"],
		},
	},
	{
		tag: ["monday", "tuesday"],
		start_time: "8:50",
		end_time: "9:20",
		subject: "AP",
		grade: "grade-2",
		meet_link_default: "",
		teacher: {
			name: "Marianne",
			meet_links: ["//meet.google.com/nza-qjxn-qds"],
		},
	},
	{
		tag: ["monday", "tuesday"],
		start_time: "9:30",
		end_time: "10:00",
		subject: "Filipino",
		grade: "grade-2",
		meet_link_default: "",
		teacher: {
			name: "Kris",
			meet_links: ["//meet.google.com/zpb-dtec-adf"],
		},
	},
	{
		tag: ["wednesday", "thursday"],
		start_time: "7:30",
		end_time: "8:00",
		subject: "MTB",
		grade: "grade-2",
		meet_link_default: "",
		teacher: {
			name: "Ann",
			meet_links: ["//meet.google.com/phk-auwc-ewa"],
		},
	},
	{
		tag: ["wednesday", "thursday"],
		start_time: "8:10",
		end_time: "8:40",
		subject: "Science",
		grade: "grade-2",
		meet_link_default: "",
		teacher: {
			name: "Marianne",
			meet_links: ["//meet.google.com/nza-qjxn-qds"],
		},
	},
	{
		tag: ["wednesday", "thursday"],
		start_time: "8:50",
		end_time: "9:20",
		subject: "MAPEH",
		grade: "grade-2",
		meet_link_default: "",
		teacher: {
			name: "Ayen",
			meet_links: ["//meet.google.com/jce-qvxe-rom"],
		},
	},
	{
		tag: ["wednesday", "thursday"],
		start_time: "9:30",
		end_time: "10:00",
		subject: "ESP",
		grade: "grade-2",
		meet_link_default: "",
		teacher: {
			name: "Ann",
			meet_links: ["//meet.google.com/phk-auwc-ewa"],
		},
	},
];

export default (state = INITIAL_STATE, { payload, type }) => {
	switch (type) {
		case "SET_CLASS":
			let { subjects } = payload;
			return [...subjects];
		case "SET_GRADE_LEVEL":
			let { grade } = payload;
			return [...grade];
		default:
			return state;
	}
};
