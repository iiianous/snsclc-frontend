import moment from "moment";

export const setDay = (dayParam) => {
	let getDay = moment().format("dddd");
	let day = getDay.toLocaleLowerCase();

	if (dayParam?.day) {
		day = dayParam.day.toLocaleLowerCase()
	}

	return {
		type: "SET_DAY",
		payload: { day },
	};
};

export const setGrade = (payload = {}) => {
	return {
		type: "SET_GRADE",
		payload: { grade: payload.grade },
	};
};
