import React, { useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import queryString from "query-string";

import Main from "../layout/Main";
import ClassDetails from "../ClassDetails";
import NoClass from "../NoClass";

import { setClass } from "../../redux/actions/gradeOneActions";
import { setDay, setGrade } from "../../redux/actions/settingsActions";

import ModalContext from "../../context/modal-context";

const GradeOne = ({ match }) => {
	const data = useSelector((state) => state.gradeOneReducer);
	const todayIs = useSelector((state) => state.settingsReducer.day);
	const whatGrade = useSelector((state) => state.settingsReducer.grade);
	const dispatch = useDispatch();
	const location = useLocation();
	let { toggleModal } = useContext(ModalContext);

	const queryStr = useLocation();
	const result = queryString.parse(queryStr.search);

	useEffect(() => {
		const setDayEveryMinutes = 300000; // 5 minutes

		dispatch(setDay(result));

		const setDayId = setInterval(() => {
			dispatch(setDay());
		}, setDayEveryMinutes);

		return () => {
			clearInterval(setDayId);
		};
	}, [dispatch]);

	useEffect(() => {
		const grade = { grade: match.params.grade };
		dispatch(setClass(data, match.params.grade));
		dispatch(setGrade(grade));

		return () => {
			dispatch(setClass(data));
		};
	}, [location, match, dispatch]);

	return (
		<Main>
			<div className="w-full pt-5 md:pt-10 text-center">
				<h1 className="text-2xl md:text-3xl font-bold md:font-normal mb-1 md:mb-3 uppercase">
					{whatGrade}
				</h1>
				<h1 className="text-2xl mb-4 md:mb-6">
					Today is <span className="capitalize underline">{todayIs}</span>
				</h1>
				{/* <pre>{JSON.stringify(data, null, 2)}</pre> */}
				{data.length ? (
					data.map((item, index) => (
						<ClassDetails
							item={item}
							key={index}
							useLink={true}
							toggleModal={toggleModal}
						/>
					))
				) : (
					<NoClass width="300" height="300" />
				)}
			</div>
		</Main>
	);
};

export default GradeOne;
