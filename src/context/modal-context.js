import { createContext } from "react";

const ModalContext = createContext({
	teacher: false,
	subject: false,
	value: null,
});

export default ModalContext;
