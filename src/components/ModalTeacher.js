import React, { useContext, useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import axios from "axios";

import ModalContext from "../context/modal-context";

function Modal({ children, value }) {
	const [inputMeetLink, setInputMeetLink] = useState(["meet_link_0"]);
	const [editValue, setEditValue] = useState(null);
	let { toggleModal } = useContext(ModalContext);
	const { register, errors, handleSubmit } = useForm();

	const onSubmit = async (_data) => {
		console.log("editValue", editValue, _data);

		try {
			const result = await axios.put(
				`http://localhost:5000/api/teachers/${editValue.teacher._id}`,
				_data
			);
			console.log("result", result.data);
		} catch (error) {}

		toggleModal(false);
	};

	const createLink = () => {
		let text = "meet_link_" + inputMeetLink.length;
		setInputMeetLink([...inputMeetLink, text]);
		console.log({ inputMeetLink });
	};

	useEffect(() => {
		// console.log({ value });
		setEditValue(value);
	}, []);

	return (
		<div
			style={{ background: "rgba(0,0,0,.5)" }}
			className="w-full bg-gray-500 flex items-center justify-center top-0 absolute h-full"
		>
			<div className="w-1/2 bg-white mx-auto opacity-100 p-6 rounded-md">
				<pre>{JSON.stringify(value, null, 2)}</pre>

				<div className="flex items-end justify-end">
					<span
						className="p-4 bg-gray-100 inline-block cursor-pointer"
						onClick={() => toggleModal({ teacher: false })}
					>
						x close
					</span>
				</div>
				<div className="flex flex-row my-10">
					<h1 className="text-2xl">Create Teacher</h1>
					<form onSubmit={handleSubmit(onSubmit)} className="w-2/3 mx-auto">
						<label className="block" htmlFor="name">
							Teacher name
						</label>
						<input
							type="text"
							name="name"
							placeholder="Teacher name"
							className="p-3 w-full mt-2 bg-gray-200"
							defaultValue={value.teacher.name}
							ref={register({ required: true })}
						/>
						{errors.name && (
							<p className="text-red-700 py-2">Teacher name is required.</p>
						)}
						<label className="block mt-5" htmlFor="meet_link">
							Meet Link
						</label>
						<div
							onClick={() => createLink()}
							className="bg-orange-600 w-full p-4 rounded-sm uppercase mt-5"
						>
							Add Link
						</div>
						{inputMeetLink.map((item, indx) => (
							<input
								key={indx}
								type="text"
								name={"meet_link_" + indx}
								placeholder="https://meet.google.com/xxxxxxxxx"
								className="p-3 w-full mt-2 bg-gray-200"
								ref={register({ required: true })}
							/>
						))}

						{/* {errors.meet_link && (
							<p className="text-red-700 py-2">Meet link URL is required.</p>
						)} */}
						{/* <select
							className="p-3 w-full mt-2 bg-gray-200 pr-2"
							name="default"
							ref={register({ required: true })}
						>
							{value &&
								value.teacher.meet_links.map((itemLink, index) => (
									<option value={itemLink} key={index} className="pr-2">
										{itemLink}
									</option>
								))}
						</select> */}
						<input
							className="bg-orange-600 w-full p-4 rounded-sm uppercase mt-5"
							type="submit"
						/>
						{/* <button
							className="bg-orange-600 w-full p-4 rounded-sm uppercase mt-5"
							type="submit"
						>
							Submit_
						</button> */}
					</form>
				</div>
			</div>
		</div>
	);
}

export default Modal;
