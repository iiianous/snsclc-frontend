import React, { useState, useContext, useEffect } from "react";
import { useForm } from "react-hook-form";

import ModalContext from "../context/modal-context";

function ModalSubject({ value }) {
	const [editSubject, setEditSubject] = useState({
		subject: "",
		start_time: "",
		end_time: "",
		grade: "",
		tag: "",
		meet_link_default: "",
		teacher: {
			meet_links: [],
		},
	});

	useEffect(() => {
		console.log({ value });
		setEditSubject({ ...value });

		return () => {
			console.log("clean up!");
		};
	}, [value]);

	let { modal, toggleModal } = useContext(ModalContext);
	const { register, errors, handleSubmit } = useForm();

	const onSubmit = (data) => {
		console.log(data);
		toggleModal(false);
	};

	return (
		<div
			style={{ background: "rgba(0,0,0,.5)" }}
			className="w-full bg-gray-500 flex items-center justify-center top-0 absolute h-full"
		>
			<div className="w-1/2 bg-white mx-auto opacity-100 p-6 rounded-md">
				<div className="flex items-end justify-end">
					<span
						className="p-4 bg-gray-100 inline-block cursor-pointer"
						onClick={() => toggleModal({ ...modal, subject: false })}
					>
						x close
					</span>
				</div>
				<div className="flex flex-row my-10">
					<h1>Subject Modal</h1>
					<form onSubmit={handleSubmit(onSubmit)} className="w-2/3 mx-auto">
						<label className="block" htmlFor="subject">
							Subject
						</label>
						<input
							type="text"
							name="subject"
							placeholder="Teacher name"
							className="p-3 w-full mt-2 bg-gray-200"
							ref={register({ required: true })}
							defaultValue={editSubject.subject}
						/>
						<div className="mt-2 flex flex-row">
							<div className="flex-1 mr-2">
								<label className="block" htmlFor="start_time">
									Start Time
								</label>
								<input
									type="text"
									name="start_time"
									data-placeholder="Teacher name"
									className="p-3 w-full mt-2 bg-gray-200"
									ref={register({ required: true })}
									defaultValue={editSubject.start_time}
								/>
							</div>
							<div className="flex-1 ml-2">
								<label className="block" htmlFor="end_time">
									End Time
								</label>
								<input
									type="text"
									name="end_time"
									data-placeholder="Teacher name"
									className="p-3 w-full mt-2 bg-gray-200"
									ref={register({ required: true })}
									defaultValue={editSubject.end_time}
								/>
							</div>
						</div>
						<label className="block mt-5" htmlFor="grade">
							Grade
						</label>
						<input
							type="text"
							name="grade"
							placeholder="https://meet.google.com/xxxxxxxxx"
							className="p-3 w-full mt-2 bg-gray-200"
							ref={register({ required: true })}
							defaultValue={editSubject.grade}
						/>
						<label className="block mt-5" htmlFor="tag">
							Class day
						</label>
						<select
							className="p-3 w-full mt-2 bg-gray-200"
							name="tag"
							ref={register({ required: true })}
							defaultValue={editSubject.tag}
						>
							<option value="monday">Monday and Tuesday</option>
							<option value="wednesday">Wednesday and Thursday</option>
						</select>
						<select
							className="p-3 w-full mt-2 bg-gray-200"
							name="default"
							ref={register({ required: true })}
						>
							{editSubject &&
								editSubject.teacher.meet_links.map((itemLink, index) => (
									<option value={itemLink} key={index}>
										{itemLink}
									</option>
								))}
						</select>
						<button
							className="bg-orange-600 w-full p-4 rounded-sm uppercase mt-5 text-white"
							type="submit"
						>
							Submit
						</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default ModalSubject;
