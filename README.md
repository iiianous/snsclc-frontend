# SNSCLC Online School 2021-22 - Google Meet class links

https://snsclc-2020.firebaseapp.com/

The web application is just a consolidated Google Meet classes links provided by the teacher.
All Google Meet links are pre-arranged according to the teacher, subject and time.
This is to streamline student's access to subject or class without going back and forth to teacher's Google Meet class page.

## How to use

a.) Student's email account (Gmail) should be logged-in.

## How it works

Automatically detects school day e.g (Monday or Wednesday etc) and compile all subjects and time sequentially.

## Mock specific day to preview classes
EX: To mock Monday class add "?day=monday" at the end of the URL.

https://snsclc-2020.firebaseapp.com/school/grade-1?day=monday


