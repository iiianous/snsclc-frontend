import React, { useState, useContext } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Home from "../components/page/Home";
import GradeOne from "../components/page/GradeOne";
// import AddTeacher from "../components/page/AddTeacher";
import Admin from "../components/page/Admin";
import Subjects from "../components/page/Subjects";

import ModalContext from "../context/modal-context";

const AppRouter = () => {
	const [modal, toggleModal] = useState({
		teacher: false,
		subject: false,
	});

	return (
		<BrowserRouter>
			<Switch>
				<ModalContext.Provider value={{ modal, toggleModal }}>
					<Route exact={true} path="/" component={Home} />
					<Route exact={true} path="/school/:grade" component={GradeOne} />
					<Route path="/admin" component={Admin} />
					<Route path="/subjects" component={Subjects} />
					{/* <Route path="/add-teacher" component={AddTeacher} /> */}
				</ModalContext.Provider>
			</Switch>
		</BrowserRouter>
	);
};

export default AppRouter;
