import React, { useContext } from "react";
import { useForm } from "react-hook-form";

import ModalContext from "../context/modal-context";

function Modal({ children }) {
	let { showModal, toggleModal } = useContext(ModalContext);
	const { register, errors, handleSubmit } = useForm();

	const onSubmit = (data) => {
		console.log(data);
		toggleModal(false);
	};

	return (
		<div
			style={{ background: "rgba(0,0,0,.5)" }}
			className="w-full bg-gray-500 flex items-center justify-center top-0 absolute h-full"
		>
			<div className="w-1/2 bg-white mx-auto opacity-100 p-6 rounded-md">
				<div className="flex items-end justify-end">
					<span
						className="p-4 bg-gray-100 inline-block cursor-pointer"
						onClick={() => toggleModal(false)}
					>
						x close
					</span>
				</div>
				<div className="flex flex-row my-10">{children}</div>
			</div>
		</div>
	);
}

export default Modal;
