import React, { useContext } from "react";

import { ReactComponent as Iconlink } from "../assets/images/out.svg";
import { ReactComponent as IconWatch } from "../assets/images/stopwatch.svg";

const ClassDetails = ({ item, useLink, toggleModal }) => {
	const {
		start_time,
		end_time,
		meet_link_default,
		teacher: { name, meet_links },
	} = item;

	// console.log({ item });

	// useEffect(() => {
	// 	let beginTime, stopTime, baseTime;

	// 	let isStartingId = setInterval(function () {
	// 		beginTime = moment("10:50 AM", "HH:mm A");
	// 		stopTime = moment("11:20 AM", "HH:mm A");

	// 		baseTime = moment("10:51 AM").format("HH:mm A");
	// 		let result = moment(baseTime).isBetween(beginTime, stopTime);

	// 		console.log(result, baseTime, beginTime, stopTime);
	// 	}, 1000);

	// 	return () => {
	// 		clearInterval(isStartingId);
	// 	};
	// }, []);

	const getMeetLink = () => {
		return !!meet_link_default ? meet_link_default : meet_links[0];
	};

	return (
		<>
			{useLink ? (
				<a
					href={useLink ? getMeetLink() : null}
					onClick={() =>
						toggleModal({ teacher: false, subject: false, value: item })
					}
					target="_blank"
					rel="noopener noreferrer"
					className="mt-3 block"
				>
					<div className="w-full flex flex-row justify-center items-center bg-white rounded-md p-3 hover:bg-blue-200">
						<div className="flex-1 flex items-center justify-start">
							<div className="w-5 h-5 md:w-10 md:h-10 mr-2 md:mr-3">
								<IconWatch className="w-full" />
							</div>
							<p className="text-sm sm:text-base md:text-3xl">
								{start_time} - {end_time} AM
							</p>
						</div>
						<div className="flex-1 flex flex-col items-start ml-1 text-left">
							<p className="text-xl font-bold md:text-3xl">{item.subject}</p>
							<p className="text-xs sm:text-sm md:text-lg">Teacher {name}</p>
						</div>
						<div className="flex flex-col items-end text-left">
							<Iconlink width="18" className="mr-4" />
						</div>
					</div>
				</a>
			) : (
				<div
					onClick={() =>
						toggleModal({ teacher: false, subject: true, value: item })
					}
					className="mt-3 block cursor-pointer"
				>
					<div className="w-full flex flex-row justify-center items-center bg-white rounded-md p-3 hover:bg-blue-200">
						<div className="flex-1 flex items-center justify-start">
							<div className="w-5 h-5 md:w-10 md:h-10 mr-2 md:mr-3">
								<IconWatch className="w-full" />
							</div>
							<p className="text-sm sm:text-base md:text-3xl">
								{start_time} - {end_time} AM
							</p>
						</div>
						<div className="flex-1 flex flex-col items-start ml-1 text-left">
							<p className="text-xl font-bold md:text-3xl">{item.subject}</p>
							<p className="text-xs sm:text-sm md:text-lg">Teacher {name}</p>
						</div>
						<div className="flex flex-col items-end text-left">
							<Iconlink width="18" className="mr-4" />
						</div>
					</div>
				</div>
			)}
		</>
	);
};

export default ClassDetails;
