import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import moment from "moment";

import useNavigator from "./utils/useNavigator";
import { ReactComponent as IconSchool } from "../../src/assets/images/english.svg";

import Loading from "../components/Loading";

const Navbar = () => {
	let [timeToday, setTimeToday] = useState("");

	useEffect(() => {
		let hour, minutes, meridiem;

		let dateId = setInterval(function () {
			hour = moment().format("h");
			minutes = moment().format("mm");
			meridiem = moment().format("A");

			setTimeToday(`${hour}:${minutes} ${meridiem}`);
		}, 1000);

		return () => {
			clearInterval(dateId);
		};
	}, []);

	let { statusText } = useNavigator();

	return (
		<>
			{statusText !== "offline" || (
				<div className="bg-red-600 text-white py-3 text-center sticky top-0">
					You are offline. Please check your internet connection.
				</div>
			)}
			<div className="flex flex- items-center justify-center shadow-md">
				<div className="py-1 text-left px-3 md:px-6 flex items-center justify-center">
					<Link to="/">
						<h1 className="text-xl md:text-3xl lg:text-4xl font-bold flex flex-row items-center justify-center">
							<div className="bg-white h-16 w-16 md:h-24 md:w-24 rounded-full flex justify-center items-center">
								<IconSchool className="w-7/12 md:w-2/3" />
							</div>
							SNSCLC <span className="invisible"> - </span>
							{/* <span className="text-base text-black uppercase hidden lg:block">
								(Online School 2020)
							</span> */}
						</h1>
					</Link>
				</div>
				<div className="py-2 md:py-4 md:flex-1 invisible"></div>
				<div className="py-2 md:py-4  flex-1 flex items-end justify-end pr-6">
					{/* <Link className="p-3" to="/admin">
						Admin
					</Link>
					<Link className="p-3" to="/subjects">
						Subject
					</Link> */}
					{!timeToday ? (
						<Loading />
					) : (
						<span className="text-base sm:text-xl md:text-2xl">
							Time: {timeToday}
						</span>
					)}
				</div>
			</div>
		</>
	);
};

export default Navbar;
