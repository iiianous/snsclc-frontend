import React from "react";
import { ReactComponent as IconLoader } from "../../src/assets/images/golden.svg";

const Loading = () => {
	return (
		<div>
			<IconLoader width="30" />
		</div>
	);
};

export default Loading;
