const INITIAL_STATE = {
	day: "",
	grade: "",
};

export default (state = INITIAL_STATE, { payload, type }) => {
	switch (type) {
		case "SET_GRADE":
			let { grade } = payload;
			return { ...state, grade };
		case "SET_DAY":
			let { day } = payload;
			console.log("PAYLOAD::::", day);
			return { ...state, day };
		default:
			return state;
	}
};
