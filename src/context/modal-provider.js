import React, { useState } from "react";
import ModalContext from "./modal-context";

const ModalProvider = ({ children }) => {
	const [showModal, toggleModal] = useState(false);
	return (
		<ModalContext.Provider value={{ showModal, toggleModal }}>
			{children}
		</ModalContext.Provider>
	);
};

export default ModalProvider;
