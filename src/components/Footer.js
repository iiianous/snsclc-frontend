import React from "react";
import useNavigator from "./utils/useNavigator";

const Footer = () => {
	let { statusText } = useNavigator();

	return (
		<div className="flex py-4 md:py-10 items-center justify-center flex-col">
			<p>
				You are:{" "}
				<span
					className={
						statusText === "online"
							? "text-green-400 capitalize font-bold"
							: "text-red-600 capitalize font-bold"
					}
				>
					{statusText}
				</span>
			</p>
			{/* <p className="text-sm md:text-base">School 2020 v.1.0</p> */}
			<p className="text-sm md:text-base">
				Class links last updated: January 20, 2021
			</p>
		</div>
	);
};

export default Footer;
