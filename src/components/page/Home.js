import React from "react";
import { Link } from "react-router-dom";

import Main from "../layout/Main";

// const Home = () => {
// 	return (
// 		<div className="flex flex-wrap">
// 			<div className="w-3/4">
// 				<h1 className="text-3xl">Div 1</h1>
// 			</div>
// 			<div className="w-3/4">
// 				<h1 className="text-3xl">Div 2</h1>
// 			</div>
// 			<div className="w-3/4">
// 				<h1 className="text-3xl">Div 3</h1>
// 			</div>
// 			<div className="flex-1">
// 				<h1 className="text-3xl">Div 4</h1>
// 			</div>
// 			<div className="flex-1">
// 				<h1 className="text-3xl">Div 5</h1>
// 			</div>
// 		</div>
// 	);
// };

const Home = () => {
	return (
		<Main>
			<div className="w-full pt-4 flex flex-col">
				<div className="flex items-center justify-center mb-5">
					<h1 className="text-xl sm:text-2xl md:text-4xl">
						What grade are you in?
					</h1>
				</div>
				<div className="flex flex-row">
					<Link to="/school/grade-1">
						<div className="flex flex-col text-center bg-white p-8 rounded-md mx-2 hover:bg-blue-200">
							<p className="text-2xl">Grade</p>
							<h2 className="text-3xl md:text-5xl">1</h2>
						</div>
					</Link>
					<Link to="/school/grade-2">
						<div className="flex flex-col text-center bg-white p-8 rounded-md mx-2 hover:bg-blue-200">
							<p className="text-2xl">Grade</p>
							<h2 className="text-3xl md:text-5xl">2</h2>
						</div>
					</Link>
				</div>
			</div>
		</Main>
	);
};

// export default Main(Home);
export default Home;
