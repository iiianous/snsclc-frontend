import React, { useEffect, useState, useContext } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { useForm } from "react-hook-form";

import Main from "../layout/Main";
import Modal from "../Modal";
import ModalContext from "../../context/modal-context";

const Admin = () => {
	let [teachers, setTeachers] = useState(null);
	let { modal, toggleModal } = useContext(ModalContext);
	const { register, errors, handleSubmit } = useForm();

	async function fetchData() {
		const { data } = await axios.get("http://localhost:5000/api/teachers/");
		setTeachers(data);
	}

	async function UpdateTeacher() {
		const { data } = await axios.get("http://localhost:5000/api/teachers/");
		setTeachers(data);
	}

	const onSubmit = (data) => {
		console.log({ data });
		toggleModal(false);
	};

	useEffect(() => {
		fetchData();
	}, []);

	const openModal = (teacher) => {
		toggleModal({ ...modal, teacher: true, value: teacher });
	};

	return (
		<Main>
			<div className="w-full pt-4 flex flex-col">
				<div className="flex items-center justify-center mb-5">
					<h1 className="text-xl sm:text-2xl md:text-4xl">All Teachers</h1>
				</div>
				<div className="flex flex-row">
					{teachers &&
						teachers.map((teacher, index) => (
							<div
								key={index}
								className="cursor-pointer"
								onClick={() => openModal({ teacher })}
							>
								<div
									data-id={teacher._id}
									className="flex flex-col text-center bg-white p-8 rounded-md mx-2 hover:bg-blue-200"
								>
									<p className="text-3xl">Teacher</p>
									<p className="text-lg">{teacher.name}</p>
								</div>
							</div>
						))}
				</div>
			</div>
		</Main>
	);
};

// export default Main(Admin);
export default Admin;
