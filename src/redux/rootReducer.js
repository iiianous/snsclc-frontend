import { combineReducers } from "redux";

import gradeOneReducer from "./reducers/gradeOneReducer";
import settingsReducer from "./reducers/settingsReducer";

const rootReducer = () =>
	combineReducers({
		gradeOneReducer,
		settingsReducer,
	});

export default rootReducer;
