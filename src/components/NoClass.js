import React from "react";

import { ReactComponent as IconNoClass } from "../assets/images/classroom.svg";

const NoClass = () => {
	return (
		<div className="h-full flex items-center justify-center pt-10 flex-col">
			<h1 className="text-4xl mb-6">You have no class today.</h1>
			<IconNoClass width="200" />
		</div>
	);
};

export default NoClass;
